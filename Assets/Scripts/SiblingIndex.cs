﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiblingIndex : MonoBehaviour
{
    [HideInInspector]
    public int index { get; set; }

    private void Start()
    {
        index = transform.GetSiblingIndex();
    }
}
