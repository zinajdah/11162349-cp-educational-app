﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ModelControl : MonoBehaviour
{
    public Canvas mainCanvas;
    public Canvas secondCanvas;

    public GameObject[] gameObjects;
    public GameObject[] labels;
    public MeshRenderer[] originalMeshObjects;
    public Transform model;

    public Button showWholeModelButton;

    [HideInInspector]
    public bool showingLabels;
    private bool outlineMode;

    [HideInInspector]
    public bool isolatingObject;

    private float lerpTime;
    public Material[] originalMeshObjectsMats;

    private static ModelControl instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        showingLabels = true;

        originalMeshObjectsMats = new Material[originalMeshObjects.Length];
        for (int i = 0; i < originalMeshObjectsMats.Length; i ++)
        {
            originalMeshObjectsMats[i] = originalMeshObjects[i].material;
            originalMeshObjects[i].material = Instantiate(originalMeshObjectsMats[i] as Material);
        }
    }

    public void SwitchModel(int btn)
    {
        SetActiveObject(gameObjects[btn]);
    }

    void SetActiveObject(GameObject gameObject)
    {
        mainCanvas.enabled = false;
        secondCanvas.enabled = true;

        gameObject.SetActive(true);
        foreach (GameObject g in gameObjects)
        {
            if (g != gameObject)
            {
                g.SetActive(false);
            }
        }

        model = gameObject.transform;
        GameObject.Find("Model_Name_Text").GetComponent<Text>().text = gameObject.name;

        foreach (GameObject g in gameObjects)
        {
            SetModelTransform(g.transform);
        }
    }

    public void ToggleLabels()
    {
        StopAllCoroutines();

        if (showingLabels)
        {
            foreach (GameObject g in labels)
            {
                for (int i = 0; i < g.transform.childCount; i ++)
                {
                    Material m = Instantiate(Resources.Load("Transparent")) as Material;

                    if (g.name.Equals("Labels_Main"))
                        g.transform.GetChild(i).GetComponentInChildren<LineRenderer>().material = m;

                    StartCoroutine(FadeLabel(1, g.transform.GetChild(i).gameObject));
                }
            }

           // FindObjectOfType<MouseOrbit>().SetCameraMode(0);
        }
        else
        {
            foreach (GameObject g in labels)
            {
                if (isolatingObject)
                {
                    if (g.name.Equals("Labels_Isolate") && g.transform.parent.GetComponent<ObjectIsolate>().isolated)
                    {
                        for (int i = 0; i < g.transform.childCount; i++)
                        {
                            StartCoroutine(FadeLabel(0, g.transform.GetChild(i).gameObject));
                        }
                    }
                }
                else
                {
                    if (g.name.Equals("Labels_Main"))
                    {
                        for (int i = 0; i < g.transform.childCount; i++)
                        {
                            StartCoroutine(FadeLabel(0, g.transform.GetChild(i).gameObject));
                        }
                    }
                }
            }
        }

        showingLabels = !showingLabels;
    }

    private void SetModelTransform(Transform t)
    {
        // Ortho
        if (showingLabels)
        {
            t.position = new Vector3(-0.33f, -3.57f, 12.93f);
            t.eulerAngles = new Vector3(11.13f, 126.5f, -1.94f);
        }
        // Perspective
        else
        {
            t.position = new Vector3(0, -1.62f, 12.35f);
            t.eulerAngles = new Vector3(0, 180, 0);
        }
    }

    // Unisolates object
    public void ShowWholeModel()
    {
        Transform p = model.GetChild(0);
        Transform cam = Camera.main.transform;
        int camChilds = cam.childCount;
        for (int n = 0; n < camChilds; n ++)
        {
            Transform c = cam.GetChild(0);
            c.SetParent(p);
            c.SetSiblingIndex(c.GetComponent<SiblingIndex>().index);
        }

        for (int i = 0; i < p.childCount; i++)
        {
            Transform part = p.GetChild(i);

            if (!part.GetComponent<ObjectIsolate>().isolated)
            {
                if (part.GetComponent<MeshRenderer>() != null)
                    StartCoroutine(FadeMaterial(part.gameObject, 0));
            }

            // Sub meshes
            for (int j = 0; j < part.transform.childCount; j++)
            {
                if (part.transform.GetChild(j).GetComponent<MeshRenderer>() != null && !part.GetComponent<ObjectIsolate>().isolated)
                {
                    StartCoroutine(FadeMaterial(part.transform.GetChild(j).gameObject, 0));
                }

                if (j == 0 && showingLabels)
                {
                    part.GetChild(0).GetChild(0).GetComponentInChildren<Button>().enabled = true;
                    StartCoroutine(FadeLabel(0, part.GetChild(0).GetChild(0).gameObject));
                }
                else if (j == part.transform.childCount - 1 && !part.GetComponent<ObjectIsolate>().isolated)
                {
                    for (int k = 0; k < part.transform.GetChild(j).childCount; k++)
                    {
                        if (part.transform.GetChild(j).GetChild(k).GetComponent<MeshRenderer>() != null)
                            StartCoroutine(FadeMaterial(part.transform.GetChild(j).GetChild(k).gameObject, 0));
                    }
                }
            }

            StartCoroutine(part.GetComponent<ObjectIsolate>().LerpToVector(part.transform, false));

            if (part.GetComponent<ObjectIsolate>().enlarge && part.GetComponent<ObjectIsolate>().isEnlarged)
            {
                part.GetComponent<ObjectIsolate>().isEnlarged = false;
            }

            // If label mode, swap to isolate labels
            if (showingLabels)
            {
                if (part.childCount > 0)
                {
                    StartCoroutine(FadeLabel(0, part.GetChild(0).gameObject));

                    if (part.GetComponent<ObjectIsolate>().isolated)
                    {
                        for (int k = 0; k < part.GetChild(1).childCount; k++)
                        {
                            Material m = Instantiate(Resources.Load("Transparent")) as Material;
                            part.GetChild(1).GetChild(k).GetComponentInChildren<LineRenderer>().material = m;
                            StartCoroutine(FadeLabel(1, part.GetChild(1).GetChild(k).gameObject));
                        }

                        part.GetComponent<ObjectIsolate>().isolated = false;
                    }
                }
            }
        }

        GameObject.Find("Model_Name_Text").GetComponent<Text>().text = model.name;
        isolatingObject = false;
        showWholeModelButton.gameObject.SetActive(false);
    }

    public void ToggleMesh()
    {
        int i = 0;
        outlineMode = !outlineMode;

        foreach (MeshRenderer o in originalMeshObjects)
        {
            o.material = outlineMode ? Instantiate(Resources.Load("Outline") as Material) : Instantiate(originalMeshObjectsMats[i] as Material);

            if (o.name.Equals("Tank") && outlineMode)
            {
                o.material.renderQueue = 4000;
            }

            i++;
        }
    }

    public IEnumerator FadeMaterial(GameObject obj, int fading)
    {
        Material mat;
        if (!outlineMode)
        {
            mat = Instantiate(Resources.Load("Fade_Mat") as Material);
            mat.CopyPropertiesFromMaterial(obj.GetComponent<MeshRenderer>().material);
            mat.SetFloat("_Mode", 2.0f);
            mat.SetFloat("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            mat.SetFloat("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            mat.SetFloat("_ZWrite", 0);
            mat.DisableKeyword("_ALPHATEST_ON");
            mat.EnableKeyword("_ALPHABLEND_ON");
            mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");

            //mat = Instantiate(Resources.Load(obj.name + "_Fade") as Material);
            obj.GetComponent<MeshRenderer>().material = mat;
        }
        else
        {
             mat = obj.GetComponent<MeshRenderer>().material;
        }

        if (fading == 0)
        {
            obj.SetActive(true);
        }

        for (float t = 0.0f; t < 1.25f; t += Time.deltaTime * 2f)
        {
            Color newMatColor = new Color();

            // fade out
            if (fading == 1)
            {
                if (!outlineMode)
                    newMatColor = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(1, 0, t));
                else
                {
                    mat.SetFloat("_BodyAlpha", Mathf.Lerp(0.125f, 0, t));
                    mat.SetFloat("_OutlineWidth", Mathf.Lerp(0.003f, 0, t));
                }
            }
            // fade in
            else
            {
                if (!outlineMode)
                    newMatColor = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(0, 1, t));
                else
                {
                    mat.SetFloat("_BodyAlpha", Mathf.Lerp(0, 0.258f, t));
                    mat.SetFloat("_OutlineWidth", Mathf.Lerp(0, 0.003f, t));
                }
            }

            if (!outlineMode)
                mat.color = newMatColor;

            yield return null;
        }

        if (fading == 0)
        {
            mat.SetFloat("_Mode", 0.0f);
            mat.SetFloat("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            mat.SetFloat("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            mat.SetFloat("_ZWrite", 1);
            mat.DisableKeyword("_ALPHATEST_ON");
        }
        else if (fading == 1)
        {
            obj.SetActive(false);
        }
    }

    public IEnumerator FadeLabel(int fading, GameObject obj)
    {
        Material mat = obj.GetComponentInChildren<LineRenderer>().material;
        Image img = obj.GetComponentInChildren<Image>();
        Text txt = obj.GetComponentInChildren<Text>();

        if (fading == 0)
        {
            obj.SetActive(true);
        }

        for (float t = 0.0f; t < 1.25f; t += Time.deltaTime * 2f)
        {
            Color newMatColor;
            Color newImgColor;
            Color newTxtColor;

            // fade out
            if (fading == 1)
            {
                newMatColor = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(1, 0, t));
                newImgColor = new Color(img.color.r, img.color.g, img.color.b, Mathf.Lerp(1, 0, t));
                newTxtColor = new Color(txt.color.r, txt.color.g, txt.color.b, Mathf.Lerp(1, 0, t));
            }
            // fade in
            else
            {
                newMatColor = new Color(mat.color.r, mat.color.g, mat.color.b, Mathf.Lerp(0, 1, t));
                newImgColor = new Color(img.color.r, img.color.g, img.color.b, Mathf.Lerp(0, 1, t));
                newTxtColor = new Color(txt.color.r, txt.color.g, txt.color.b, Mathf.Lerp(0, 1, t));
            }

            mat.color = newMatColor;
            img.color = newImgColor;
            txt.color = newTxtColor;
            yield return null;
        }

        if (fading == 1)
        {
            obj.GetComponentInChildren<Button>().interactable = false;
            obj.SetActive(false);
        }
        else
        {
            obj.GetComponentInChildren<Button>().interactable = true;
        }
    }

    public IEnumerator WaitForTransitionShowModelButton(bool on)
    {
        yield return new WaitForSeconds(0.5f);
        showWholeModelButton.gameObject.SetActive(on);
    }
}
