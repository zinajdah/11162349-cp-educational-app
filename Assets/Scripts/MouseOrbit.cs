﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MouseOrbit : MonoBehaviour
{
    public Transform target;
    public float distance = 20.0f;
    public float xSpeed = 10.0f;
    public float ySpeed = 160.0f;

    public float yMinLimit = -90f;
    public float yMaxLimit = 90f;

    public float distanceMin = 15f;
    public float distanceMax = 30f;
    public float pinchDistanceMin = 40f;
    public float pinchDistanceMax = 105f;

    public float swipeDistance;

    public bool canOrbit = true;

    [HideInInspector]
    public float x = 0.0f;

    [HideInInspector]
    public float y = 0.0f;

    private ModelControl mc;
    private float range;
    private float spinDir = 0;

    private Vector3 originalCameraPosition;
    private Quaternion originalCameraRotation;

#if UNITY_EDITOR
    private List<Vector3> touchPos = new List<Vector3>();
#else
    private List<Vector2> touchPos = new List<Vector2>();
#endif


    Camera cam;
    // Use this for initialization
    void Start()
    {
#if UNITY_EDITOR && !UNITY_ANDROID
        swipeDistance = 20f;
#else
        swipeDistance = 25f;
#endif

        cam = Camera.main;
        mc = FindObjectOfType<ModelControl>();
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        originalCameraPosition = transform.position;
        originalCameraRotation = transform.rotation;
    }

    void LateUpdate()
    {
        if (mc.showingLabels)
        {
            /*distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, 2, 7);

            cam.orthographicSize = distance;*/
            
        }
#if UNITY_EDITOR //&& !UNITY_ANDROID
        if (canOrbit)//&& !mc.showingLabels)
        {
            if (Input.GetMouseButtonDown(0))
            {
                range = 0;
            }
            else if (Input.GetMouseButton(0))
            {
                x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

                y = ClampAngle(y, yMinLimit, yMaxLimit);

                Quaternion rotation = Quaternion.Euler(y, x, 0);

                //Debug.Log("X: " + x + ", Y: " + y + ", Rot: " + rotation);
                distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, distanceMin, distanceMax);

                RaycastHit hit;
                if (Physics.Linecast(target.position, transform.position, out hit))
                {
                    distance -= hit.distance;
                }

                Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
                Vector3 position = rotation * negDistance + target.position;

                transform.rotation = rotation;
                transform.position = position;

                if (touchPos.Count < 12)
                {
                    touchPos.Add(Input.mousePosition);
                    if (touchPos.Count == 11)
                    {
                        touchPos.Clear();
                        touchPos.Add(Input.mousePosition);
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (touchPos.Count > 0)
                {
                    for (int i = touchPos.Count; i > 1; i--)
                    {
                        range = touchPos[i - 1].x - touchPos[i - 2].x;
                    }

                    if ((range > swipeDistance || range < -swipeDistance) && range != 0)
                    {
                        if (range > 0)
                        {
                            range = Mathf.Clamp(range, 0, 20);
                        }
                        else
                        {
                            range = Mathf.Clamp(range, -20, 0);
                        }
                    }
                    touchPos.Clear();
                }
            }
        }
#else
        if (canOrbit)
        {
            // Zoom
            if (Input.touchCount == 2)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                
                GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff / 20f;
                GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, pinchDistanceMin, pinchDistanceMax);
            }
            else
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    range = 0;
                }
                else if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    x += Input.GetAxis("Mouse X") * (xSpeed / 3) * distance * 0.02f;
                    y -= Input.GetAxis("Mouse Y") * (ySpeed / 2) * 0.02f;

                    y = ClampAngle(y, yMinLimit, yMaxLimit);

                    Quaternion rotation = Quaternion.Euler(y, x, 0);
                    distance = Mathf.Clamp(distance - GetComponent<Camera>().fieldOfView, pinchDistanceMin, pinchDistanceMax);

                    RaycastHit hit;
                    if (Physics.Linecast(target.position, transform.position, out hit))
                    {
                        distance -= hit.distance;
                    }

                    Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
                    Vector3 position = rotation * negDistance + target.position;

                    transform.rotation = rotation;
                    transform.position = position;

                    if (touchPos.Count < 12)
                    {
                        touchPos.Add(Input.GetTouch(0).position);
                        if (touchPos.Count == 11)
                        {
                            touchPos.Clear();
                            touchPos.Add(Input.GetTouch(0).position);
                        }
                    }
                }
                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    if (touchPos.Count > 0)
                    {
                        for (int i = touchPos.Count; i > 1; i--)
                        {
                            range = touchPos[i - 1].x - touchPos[i - 2].x;
                        }

                        if ((range > swipeDistance || range < -swipeDistance) && range != 0)
                        {
                            if (range > 0)
                            {
                                range = Mathf.Clamp(range, 0, 20);
                            }
                            else
                            {
                                range = Mathf.Clamp(range, -20, 0);
                            }
                        }
                        else
                        {
                            range = 0;
                        }

                        touchPos.Clear();
                    }
                }
            }
            
        }
#endif
    }

    private void Update()
    {
        if (range != 0)
        {
            // right spin
            if (range > 0)
            {
                range -= Time.deltaTime * 10;
                spinDir = (range / 4);
                if (range < 0)
                {
                    range = 0;
                }

            }
            // left spin
            else if (range < 0)
            {
                range += Time.deltaTime * 10;
                spinDir = (range / 4);

                if (range > 0)
                {
                    range = 0;
                }
            }
            else
            {
                spinDir = 0;
                range = 0;
            }
            //GameObject.Find("Debug").GetComponent<Text>().text = range.ToString();
            //Debug.Log("spindir: " + spinDir + ", range: " + range);

            x = transform.eulerAngles.y;
            Quaternion rotation = Quaternion.Euler(y, x + spinDir, 0);
            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;
            transform.rotation = rotation;
            transform.position = position;
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    public void SetCanOrbit(bool orbit)
    {
        if (orbit)
            canOrbit = true;
        else
            canOrbit = false;
    }

    public IEnumerator ResetCamera()
    {
        float time = 0;
        while (Camera.main.transform.position != originalCameraPosition)
        {
            time += Time.deltaTime;
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, originalCameraPosition, time);
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, originalCameraRotation, time);
            yield return null;
        }

        x = 0;
        y = 0;
    }
}